# Example Accountant CLI applications

This application takes in a list of transactions.


## How to use
```
USAGE:
    accountant [OPTIONS] <transaction_file>

ARGS:
    <transaction_file>    Path to a csv file with transaction data

OPTIONS:
    -h, --help       Print help information
    -o, --ordered    Order output by client id
    -s, --silent     Silence the output of errors. Not recommended.
    -V, --version    Print version information
```


