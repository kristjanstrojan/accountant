use rust_decimal::prelude::Decimal;
use serde::Deserialize;

#[derive(Debug, Deserialize, Copy, Clone)]
pub enum TransactionStatus {
    #[serde(rename = "deposit")]
    Deposit,
    #[serde(rename = "withdrawal")]
    Withdrawal,
    #[serde(rename = "dispute")]
    Dispute,
    #[serde(rename = "resolve")]
    Resolve,
    #[serde(rename = "chargeback")]
    Chargeback,
}

#[derive(Debug, Deserialize)]
pub struct RawTransaction {
    #[serde(rename = "type")]
    pub status: TransactionStatus,
    pub client: u16,
    pub tx: u32,
    pub amount: Option<Decimal>,
}

impl RawTransaction {
    pub fn get_formatted_amount(&self) -> Result<Decimal, &'static str> {
        // Returns fourmated am
        match self.amount {
            Some(amount) => Ok(format_decimal(amount.clone())),
            None => Err("Transaction has no amount."),
        }
    }
}

#[derive(Debug)]
pub enum Message {
    TransactionMessage(RawTransaction),
    Exit,
}

pub fn format_decimal(mut amount: Decimal) -> Decimal {
    if amount.scale() < 2 {
        amount.rescale(1);
    } else {
        amount.rescale(4);
    }
    return amount;
}
