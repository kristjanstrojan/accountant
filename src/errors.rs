use log::error;

pub fn log_transaction_error(transaction_id: u32, message: &str) {
    error!("[transaction:{}] - {}", transaction_id, message);
}

pub fn log_deserialization_error(row_num: u32, message: &str) {
    error!("[deserialization.error][row:{}] - {}", row_num, message);
}
