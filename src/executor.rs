use crate::jobs;
use tokio::sync::mpsc;

const CHANNEL_SIZE: usize = 10000;

pub async fn process(path_to_file: String, ordered: bool) {
    let (s, r) = mpsc::channel(CHANNEL_SIZE);

    tokio::spawn(async move {
        jobs::data_reader(path_to_file, s).await;
    });

    jobs::transaction_processor(r, ordered).await;
}
