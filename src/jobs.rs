use crate::common::{Message, RawTransaction};
use crate::errors;
use crate::ledger::{Client, Ledger};
use csv::{Error, ReaderBuilder, Trim};
use std::io;
use tokio::sync::mpsc;

pub async fn transaction_processor(mut r: mpsc::Receiver<Message>, ordered: bool) {
    let mut ledger = Ledger::new();
    loop {
        let k = r.recv().await.unwrap();
        match k {
            Message::TransactionMessage(s) => {
                let tid = s.tx;
                match ledger.process_transaction(s) {
                    Ok(_) => continue,
                    Err(err) => errors::log_transaction_error(tid, err),
                };
            }
            Message::Exit => {
                break;
            }
        }
    }

    let mut writer = csv::Writer::from_writer(io::stdout());

    if ordered == false {
        for client in ledger.clients.values() {
            writer.serialize(client).unwrap();
        }
    } else {
        let mut clients_vec: Vec<Client> = ledger.clients.values().cloned().collect();
        clients_vec.sort_by_key(|a| a.client);
        for client in clients_vec {
            writer.serialize(client).unwrap();
        }
    }
    writer.flush().unwrap();
}

pub async fn data_reader(path: String, s: mpsc::Sender<Message>) {
    let mut rdr = match ReaderBuilder::new()
        .flexible(true)
        .trim(Trim::All)
        .from_path(path)
    {
        Ok(reader) => reader,
        Err(err) => {
            s.try_send(Message::Exit).unwrap();
            eprintln!("Error when trying to read transaction file: {}", err);
            return ();
        }
    };

    for row in rdr.deserialize().enumerate() {
        let (i, result): (usize, Result<RawTransaction, Error>) = row;
        match result {
            Ok(transaction) => {
                s.send(Message::TransactionMessage(transaction))
                    .await
                    .expect("cannot send user");
            }
            Err(_err) => {
                errors::log_deserialization_error((i as u32) + 1, "Could not deserialize row");
                // Skip error transaction. We could write it to the error log.
                continue;
            }
        };
    }

    // Exit the thread
    s.send(Message::Exit)
        .await
        .expect("Sending exit message failed");
}
