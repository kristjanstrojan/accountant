use crate::common::{format_decimal, RawTransaction, TransactionStatus};

use rust_decimal::prelude::*;
use serde::Serialize;
use std::collections::HashMap;

struct Transaction {
    amount: Decimal,
    status: TransactionStatus,
    client_id: u16,
}

impl Transaction {
    fn new(amount: Decimal, status: TransactionStatus, client_id: u16) -> Transaction {
        return Transaction {
            amount,
            status,
            client_id,
        };
    }
}

#[derive(Clone, Debug, Serialize)]
pub struct Client {
    pub client: u16,
    available: Decimal,
    held: Decimal,
    total: Decimal,
    locked: bool,
}

impl Client {
    fn new(id: u16) -> Client {
        return Client {
            client: id,
            total: Decimal::new(0, 1),
            available: Decimal::new(0, 1),
            held: Decimal::new(0, 1),
            locked: false,
        };
    }
}

pub struct Ledger {
    transactions: HashMap<u32, Transaction>,
    pub clients: HashMap<u16, Client>,
}

impl Ledger {
    pub fn new() -> Ledger {
        return Ledger {
            clients: HashMap::new(),
            transactions: HashMap::new(),
        };
    }

    pub fn process_transaction(&mut self, transaction: RawTransaction) -> Result<(), &'static str> {
        // Creates the client record if client does not exist.
        if self.clients.contains_key(&transaction.client) == false {
            self.add_client(transaction.client);
        }

        // Process the transaction.
        let action = match transaction.status {
            TransactionStatus::Deposit => self.add_transaction(&transaction),
            TransactionStatus::Withdrawal => self.add_transaction(&transaction),
            TransactionStatus::Dispute => self.update_transaction(&transaction),
            TransactionStatus::Resolve => self.update_transaction(&transaction),
            TransactionStatus::Chargeback => self.update_transaction(&transaction),
        };

        action
    }

    fn add_transaction(&mut self, raw_transaction: &RawTransaction) -> Result<(), &'static str> {
        // If transaction already exists, we cannot process the same transaction. Log the error end
        // ignore this transaction.
        if self.transactions.contains_key(&raw_transaction.tx) {
            return Err("Cannot add transaction with duplicated id.");
        }

        // let amount = format_decimal(raw_transaction.amount.ok_or("Amount missing in transaction."));
        let amount = raw_transaction.get_formatted_amount()?;
        let client = self
            .clients
            .get_mut(&raw_transaction.client)
            .ok_or("Cannot find client.")?;

        if client.locked == true {
            return Err("Client is locked.");
        }

        match raw_transaction.status {
            TransactionStatus::Deposit => {
                client.total = format_decimal(client.total + amount);
                client.available = format_decimal(client.available + amount);
            }
            TransactionStatus::Withdrawal => {
                if client.available >= amount {
                    client.available = format_decimal(client.available - amount);
                    client.total = format_decimal(client.total - amount);
                } else {
                    return Err("Not enough available funds to process transaction.");
                }
            }
            _ => {
                return Err("Only deposit or withd");
            }
        }

        // Record transaction
        self.transactions.insert(
            raw_transaction.tx,
            Transaction::new(amount, raw_transaction.status, raw_transaction.client),
        );

        Ok(())
    }

    fn update_transaction(&mut self, raw_transaction: &RawTransaction) -> Result<(), &'static str> {
        let transaction = self
            .transactions
            .get_mut(&raw_transaction.tx)
            .ok_or("Cannot find transaction")?;
        let client = self
            .clients
            .get_mut(&raw_transaction.client)
            .ok_or("Cannot find client.")?;

        // Sanity check
        if raw_transaction.client != transaction.client_id {
            return Err("Client IDs are not matching.");
        }

        match raw_transaction.status {
            TransactionStatus::Dispute => {
                if matches!(transaction.status, TransactionStatus::Deposit) == false {
                    return Err("Only deposited transactions can be disputed.");
                }
                client.available = format_decimal(client.available - transaction.amount);
                client.held = format_decimal(client.held + transaction.amount);
                transaction.status = TransactionStatus::Dispute;
            }
            TransactionStatus::Resolve => {
                if matches!(transaction.status, TransactionStatus::Dispute) == false {
                    return Err("Only disputed transactions can be resolved.");
                }
                client.available = format_decimal(client.available + transaction.amount);
                client.held = format_decimal(client.held - transaction.amount);

                // We could set the status to "resolve" if assume transaction can be disputed only once.
                // I assumed transactions can be disputed several times.
                transaction.status = TransactionStatus::Deposit;
            }
            TransactionStatus::Chargeback => {
                if matches!(transaction.status, TransactionStatus::Dispute) == false {
                    return Err("Only disputed transactions can be chargedback.");
                }
                client.total = format_decimal(client.total - transaction.amount);
                client.held = format_decimal(client.held - transaction.amount);
                transaction.status = TransactionStatus::Chargeback;
                client.locked = true;
            }
            _ => {
                return Err("Got incorrect transaction type for update operation");
            }
        }

        Ok(())
    }

    fn add_client(&mut self, client_id: u16) {
        self.clients.insert(client_id, Client::new(client_id));
    }
}

#[cfg(test)]
mod tests {
    use crate::ledger;
    use rust_decimal::prelude::*;

    #[test]
    fn ledger_deposit() {
        let mut ledger = ledger::Ledger::new();
        let transactions = [
            // Client 1
            ledger::RawTransaction {
                client: 1,
                tx: 1,
                status: ledger::TransactionStatus::Deposit,
                amount: Decimal::from_str("10.0").ok(),
            },
            ledger::RawTransaction {
                client: 1,
                tx: 2,
                status: ledger::TransactionStatus::Deposit,
                amount: Decimal::from_str("2.0").ok(),
            },
            // Client 2
            ledger::RawTransaction {
                client: 2,
                tx: 5,
                status: ledger::TransactionStatus::Deposit,
                amount: Decimal::from_str("2.0").ok(),
            },
        ];
        for transaction in transactions {
            ledger.process_transaction(transaction).unwrap();
        }

        assert_eq!(ledger.transactions.len(), 3);

        let client_1 = ledger.clients.get(&1).unwrap();
        assert_eq!(client_1.total, Decimal::from_str("12.0").unwrap());
        assert_eq!(client_1.available, Decimal::from_str("12.0").unwrap());
        assert_eq!(client_1.held, Decimal::from_str("0.0").unwrap());
        assert_eq!(client_1.locked, false);

        let client_2 = ledger.clients.get(&2).unwrap();
        assert_eq!(client_2.total, Decimal::from_str("2.0").unwrap());
        assert_eq!(client_2.available, Decimal::from_str("2.0").unwrap());
        assert_eq!(client_2.held, Decimal::from_str("0.0").unwrap());
        assert_eq!(client_2.locked, false);
    }

    #[test]
    fn ledger_withdrawal() {
        let mut ledger = ledger::Ledger::new();
        let transactions = [
            // Client 1
            ledger::RawTransaction {
                client: 1,
                tx: 1,
                status: ledger::TransactionStatus::Deposit,
                amount: Decimal::from_str("10.0").ok(),
            },
            ledger::RawTransaction {
                client: 1,
                tx: 2,
                status: ledger::TransactionStatus::Withdrawal,
                amount: Decimal::from_str("2.0").ok(),
            },
            // Client 2
            ledger::RawTransaction {
                client: 2,
                tx: 3,
                status: ledger::TransactionStatus::Deposit,
                amount: Decimal::from_str("2.0").ok(),
            },
        ];
        for transaction in transactions {
            ledger.process_transaction(transaction).unwrap();
        }

        // One transaction is ignored as it tries to witdrawn more than is
        // available.
        assert_eq!(ledger.transactions.len(), 3);

        let client_1 = ledger.clients.get(&1).unwrap();
        assert_eq!(client_1.total, Decimal::from_str("8.0").unwrap());
        assert_eq!(client_1.available, Decimal::from_str("8.0").unwrap());
        assert_eq!(client_1.held, Decimal::from_str("0.0").unwrap());
        assert_eq!(client_1.locked, false);

        let client_2 = ledger.clients.get(&2).unwrap();
        assert_eq!(client_2.total, Decimal::from_str("2.0").unwrap());
        assert_eq!(client_2.available, Decimal::from_str("2.0").unwrap());
        assert_eq!(client_2.held, Decimal::from_str("0.0").unwrap());
        assert_eq!(client_2.locked, false);
    }

    #[test]
    fn ledger_withdrawal_not_enough_funds() {
        let mut ledger = ledger::Ledger::new();

        ledger
            .process_transaction(ledger::RawTransaction {
                client: 1,
                tx: 1,
                status: ledger::TransactionStatus::Deposit,
                amount: Decimal::from_str("10.0").ok(),
            })
            .unwrap();

        let failed_transaction = ledger.process_transaction(ledger::RawTransaction {
            client: 1,
            tx: 2,
            status: ledger::TransactionStatus::Withdrawal,
            amount: Decimal::from_str("11.0").ok(),
        });
        assert!(failed_transaction.is_err());
        assert_eq!(ledger.transactions.len(), 1);
    }

    #[test]
    fn ledger_failed_add_transactions() {
        const TRANSACTION_ID: u32 = 1;

        let mut ledger = ledger::Ledger::new();

        ledger
            .process_transaction(ledger::RawTransaction {
                client: 1,
                tx: TRANSACTION_ID,
                status: ledger::TransactionStatus::Deposit,
                amount: Decimal::from_str("10.0").ok(),
            })
            .unwrap();

        let failed_transactions = [
            // Duplicated ID
            ledger::RawTransaction {
                client: 1,
                tx: TRANSACTION_ID,
                status: ledger::TransactionStatus::Deposit,
                amount: Decimal::from_str("10.0").ok(),
            },
            ledger::RawTransaction {
                client: 1,
                tx: TRANSACTION_ID,
                status: ledger::TransactionStatus::Withdrawal,
                amount: Decimal::from_str("10.0").ok(),
            },
            // No amount
            ledger::RawTransaction {
                client: 1,
                tx: TRANSACTION_ID,
                status: ledger::TransactionStatus::Deposit,
                amount: None,
            },
        ];

        for failed_transaction in failed_transactions {
            assert!(ledger.process_transaction(failed_transaction).is_err());
        }

        assert_eq!(ledger.transactions.len(), 1);
    }

    #[test]
    fn ledger_dispute_transaction_sanity() {
        let mut ledger = ledger::Ledger::new();
        let transactions = [
            // Client 1
            ledger::RawTransaction {
                client: 1,
                tx: 1,
                status: ledger::TransactionStatus::Deposit,
                amount: Decimal::from_str("10.0").ok(),
            },
            ledger::RawTransaction {
                client: 1,
                tx: 2,
                status: ledger::TransactionStatus::Deposit,
                amount: Decimal::from_str("2.0").ok(),
            },
            ledger::RawTransaction {
                client: 1,
                tx: 1,
                status: ledger::TransactionStatus::Dispute,
                amount: None,
            },
        ];
        for transaction in transactions {
            ledger.process_transaction(transaction).unwrap();
        }

        // One transaction is ignored as it tries to witdrawn more than is
        // available.
        assert_eq!(ledger.transactions.len(), 2);

        let client_1 = ledger.clients.get(&1).unwrap();
        assert_eq!(client_1.total, Decimal::from_str("12.0").unwrap());
        assert_eq!(client_1.available, Decimal::from_str("2.0").unwrap());
        assert_eq!(client_1.held, Decimal::from_str("10.0").unwrap());
        assert_eq!(client_1.locked, false);
    }

    #[test]
    fn ledger_dispute_transactions_should_fail() {
        let mut ledger = ledger::Ledger::new();
        ledger
            .process_transaction(
                // Client 1
                ledger::RawTransaction {
                    client: 1,
                    tx: 1,
                    status: ledger::TransactionStatus::Deposit,
                    amount: Decimal::from_str("10.0").ok(),
                },
            )
            .unwrap();

        assert_eq!(ledger.transactions.len(), 1);

        let failed_transactions = [
            // Client IDs are not matching
            ledger::RawTransaction {
                client: 2,
                tx: 1,
                status: ledger::TransactionStatus::Dispute,
                amount: Decimal::from_str("10.0").ok(),
            },
            // Disputing non existent transaction.
            ledger::RawTransaction {
                client: 1,
                tx: 2,
                status: ledger::TransactionStatus::Dispute,
                amount: Decimal::from_str("10.0").ok(),
            },
        ];

        for transaction in failed_transactions {
            assert!(ledger.process_transaction(transaction).is_err())
        }

        assert_eq!(ledger.transactions.len(), 1);
    }

    #[test]
    fn ledger_resolve_sanity() {
        let mut ledger = ledger::Ledger::new();
        let transactions = [
            // Client 1
            ledger::RawTransaction {
                client: 1,
                tx: 1,
                status: ledger::TransactionStatus::Deposit,
                amount: Decimal::from_str("10.0").ok(),
            },
            ledger::RawTransaction {
                client: 1,
                tx: 2,
                status: ledger::TransactionStatus::Deposit,
                amount: Decimal::from_str("2.0").ok(),
            },
            ledger::RawTransaction {
                client: 1,
                tx: 1,
                status: ledger::TransactionStatus::Dispute,
                amount: None,
            },
        ];
        for transaction in transactions {
            ledger.process_transaction(transaction).unwrap();
        }

        // One transaction is ignored as it tries to witdrawn more than is
        // available.
        assert_eq!(ledger.transactions.len(), 2);

        let client_1 = ledger.clients.get(&1).unwrap();
        assert_eq!(client_1.total, Decimal::from_str("12.0").unwrap());
        assert_eq!(client_1.available, Decimal::from_str("2.0").unwrap());
        assert_eq!(client_1.held, Decimal::from_str("10.0").unwrap());
        assert_eq!(client_1.locked, false);

        // Resolve the dispute
        ledger
            .process_transaction(ledger::RawTransaction {
                client: 1,
                tx: 1,
                status: ledger::TransactionStatus::Resolve,
                amount: None,
            })
            .unwrap();

        // Transaction is not commited, just mutated.
        let client_1 = ledger.clients.get(&1).unwrap();
        assert_eq!(ledger.transactions.len(), 2);
        assert_eq!(client_1.total, Decimal::from_str("12.0").unwrap());
        assert_eq!(client_1.available, Decimal::from_str("12.0").unwrap());
        assert_eq!(client_1.held, Decimal::from_str("0.0").unwrap());
        assert_eq!(client_1.locked, false);
    }

    #[test]
    fn ledger_resolve_failed() {
        let mut ledger = ledger::Ledger::new();
        let transactions = [
            // Client 1
            ledger::RawTransaction {
                client: 1,
                tx: 1,
                status: ledger::TransactionStatus::Deposit,
                amount: Decimal::from_str("12.0").ok(),
            },
            ledger::RawTransaction {
                client: 1,
                tx: 2,
                status: ledger::TransactionStatus::Withdrawal,
                amount: Decimal::from_str("2.0").ok(),
            },
            ledger::RawTransaction {
                client: 1,
                tx: 1,
                status: ledger::TransactionStatus::Dispute,
                amount: None,
            },
            ledger::RawTransaction {
                client: 1,
                tx: 3,
                status: ledger::TransactionStatus::Deposit,
                amount: Decimal::from_str("12.0").ok(),
            },
            // Client 2
            ledger::RawTransaction {
                client: 2,
                tx: 4,
                status: ledger::TransactionStatus::Deposit,
                amount: Decimal::from_str("10.0").ok(),
            },
            ledger::RawTransaction {
                client: 2,
                tx: 5,
                status: ledger::TransactionStatus::Withdrawal,
                amount: Decimal::from_str("2.0").ok(),
            },
            ledger::RawTransaction {
                client: 2,
                tx: 4,
                status: ledger::TransactionStatus::Dispute,
                amount: None,
            },
        ];
        for transaction in transactions {
            ledger.process_transaction(transaction).unwrap();
        }
        let client_1 = ledger.clients.get(&1).unwrap();
        assert_eq!(client_1.total, Decimal::from_str("22.0").unwrap());
        assert_eq!(client_1.available, Decimal::from_str("10.0").unwrap());
        assert_eq!(client_1.held, Decimal::from_str("12.0").unwrap());
        assert_eq!(client_1.locked, false);
        let client_2 = ledger.clients.get(&2).unwrap();
        assert_eq!(client_2.total, Decimal::from_str("8.0").unwrap());
        assert_eq!(client_2.available, Decimal::from_str("-2.0").unwrap());
        assert_eq!(client_2.held, Decimal::from_str("10.0").unwrap());
        assert_eq!(client_2.locked, false);

        let failed_transactions = [
            // Client does not exist
            ledger::RawTransaction {
                client: 2,
                tx: 1,
                status: ledger::TransactionStatus::Resolve,
                amount: Decimal::from_str("10.0").ok(),
            },
            // Client ID does not match
            ledger::RawTransaction {
                client: 2,
                tx: 1,
                status: ledger::TransactionStatus::Resolve,
                amount: Decimal::from_str("2.0").ok(),
            },
            // Transaction was not disputed
            ledger::RawTransaction {
                client: 1,
                tx: 2,
                status: ledger::TransactionStatus::Resolve,
                amount: Decimal::from_str("2.0").ok(),
            },
            ledger::RawTransaction {
                client: 1,
                tx: 4,
                status: ledger::TransactionStatus::Resolve,
                amount: Decimal::from_str("2.0").ok(),
            },
        ];

        for failed_transaction in failed_transactions {
            assert!(ledger.process_transaction(failed_transaction).is_err())
        }

        // Assert nothing changed
        let client_1 = ledger.clients.get(&1).unwrap();
        assert_eq!(client_1.total, Decimal::from_str("22.0").unwrap());
        assert_eq!(client_1.available, Decimal::from_str("10.0").unwrap());
        assert_eq!(client_1.held, Decimal::from_str("12.0").unwrap());
        assert_eq!(client_1.locked, false);
        let client_2 = ledger.clients.get(&2).unwrap();
        assert_eq!(client_2.total, Decimal::from_str("8.0").unwrap());
        assert_eq!(client_2.available, Decimal::from_str("-2.0").unwrap());
        assert_eq!(client_2.held, Decimal::from_str("10.0").unwrap());
        assert_eq!(client_2.locked, false);
    }

    #[test]
    fn ledger_chargeback_sanity() {
        let mut ledger = ledger::Ledger::new();
        let transactions = [
            // Client 1
            ledger::RawTransaction {
                client: 1,
                tx: 1,
                status: ledger::TransactionStatus::Deposit,
                amount: Decimal::from_str("10.0").ok(),
            },
            ledger::RawTransaction {
                client: 1,
                tx: 2,
                status: ledger::TransactionStatus::Deposit,
                amount: Decimal::from_str("2.0").ok(),
            },
            ledger::RawTransaction {
                client: 1,
                tx: 1,
                status: ledger::TransactionStatus::Dispute,
                amount: None,
            },
        ];
        for transaction in transactions {
            ledger.process_transaction(transaction).unwrap();
        }

        // One transaction is ignored as it tries to witdrawn more than is
        // available.
        assert_eq!(ledger.transactions.len(), 2);

        let client_1 = ledger.clients.get(&1).unwrap();
        assert_eq!(client_1.total, Decimal::from_str("12.0").unwrap());
        assert_eq!(client_1.available, Decimal::from_str("2.0").unwrap());
        assert_eq!(client_1.held, Decimal::from_str("10.0").unwrap());
        assert_eq!(client_1.locked, false);

        // Resolve the dispute
        ledger
            .process_transaction(ledger::RawTransaction {
                client: 1,
                tx: 1,
                status: ledger::TransactionStatus::Chargeback,
                amount: None,
            })
            .unwrap();

        // Transaction is not commited, just mutated.
        let client_1 = ledger.clients.get(&1).unwrap();
        assert_eq!(ledger.transactions.len(), 2);
        assert_eq!(client_1.total, Decimal::from_str("2.0").unwrap());
        assert_eq!(client_1.available, Decimal::from_str("2.0").unwrap());
        assert_eq!(client_1.held, Decimal::from_str("0.0").unwrap());
        assert_eq!(client_1.locked, true);
        // Assert cannot add transactions to the locekd client
        assert!(ledger
            .process_transaction(ledger::RawTransaction {
                client: 1,
                tx: 1,
                status: ledger::TransactionStatus::Deposit,
                amount: Decimal::from_str("10.0").ok(),
            })
            .is_err());
    }
}
