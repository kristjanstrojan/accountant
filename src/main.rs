mod common;
mod errors;
mod executor;
mod jobs;
mod ledger;

use clap::{Arg, Command};
use env_logger;
use log;

#[tokio::main]
async fn main() {
    let matches = Command::new("accountant")
        .version("0.1.0")
        .arg(
            Arg::new("transaction_file")
                .takes_value(true)
                .required(true)
                .help("Path to a csv file with transaction data"),
        )
        .arg(
            Arg::new("silent")
                .short('s')
                .long("silent")
                .help("Silence the output of errors. Not recommended."),
        )
        .arg(
            Arg::new("ordered")
                .short('o')
                .long("ordered")
                .help("Order output by client id"),
        )
        .get_matches();

    // Get the path of the transaction csv file.
    let path_to_file = matches.value_of("transaction_file").unwrap().to_string();

    // Set the verbosity of the logger.
    let silent = matches.is_present("silent");

    let ordered = matches.is_present("ordered");

    // Set the logging level.
    let log_level = match silent {
        true => log::LevelFilter::Off,
        _ => log::LevelFilter::Error,
    };
    env_logger::Builder::new()
        .filter_level(log_level)
        .try_init()
        .unwrap_or_else(|e| {
            eprintln!("Could not create a logger: {}", e);
        });

    executor::process(path_to_file.to_string(), ordered).await;
}
