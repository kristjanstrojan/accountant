use assert_cmd::prelude::*;
use predicates::prelude::*;
use std::process::Command;

const EXPECTED_OUTPUT_TEST_1: &str = r#"client,available,held,total,locked
1,1.5,0.0,1.5,false
2,2.0,0.0,2.0,false
"#;

#[test]
fn cli1() -> Result<(), Box<dyn std::error::Error>> {
    let mut cmd = Command::cargo_bin("accountant")?;

    cmd.arg("tests/artefacts/test1.csv").arg("-s").arg("-o");
    cmd.assert().stdout(predicate::eq(EXPECTED_OUTPUT_TEST_1));

    Ok(())
}

const EXPECTED_OUTPUT_TEST_2: &str = r#"client,available,held,total,locked
1,0.5,1.0,1.5,false
2,2.0,0.0,2.0,false
3,10.0,0.0,10.0,false
4,10.0,0.0,10.0,true
8,0.0,0.0,0.0,false
"#;

#[test]
fn cli2() -> Result<(), Box<dyn std::error::Error>> {
    let mut cmd = Command::cargo_bin("accountant")?;

    cmd.arg("tests/artefacts/test2.csv").arg("-s").arg("-o");
    cmd.assert().stdout(predicate::eq(EXPECTED_OUTPUT_TEST_2));

    Ok(())
}
